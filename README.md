# HelloWorld



## Breve informação / Brief information

[PT-BR]
Repositório voltado a armazenamento de "Hello, World's".

Projetos sem o prévio intuito de lucro ou lecionação.

Neste repositório, apenas projetos contendo o antigo e querido "Hello, World!".


[EN-US]
Repository aimed at storing "Hello, World's" projects.

Projects without the prior intention of profit or teaching.

In this repository, only projects containing the old and beloved "Hello, World!".



## Softwares e plataformas usados / Used softwares and platforms

[PT-BR]


- [ ] Visual Studio Code, GNU Nano e muitos outros editores de texto - Para edição de códigos.

- [ ] Git - Para controle de versões dos projetos.

- [ ] GitLab - Para gerenciar repositórios.


[EN-US]


- [ ] Visual Studio Code, GNU Nano and many other text editors - For editing code.

- [ ] Git - For project version control.

- [ ] GitLab - For managing repositories.



## Sobre a visibilidade / About the visibility

[PT-BR]
Mudanças de visibilidade (privado ou público) podem ser aleatórias.


[EN-US]
Visibility changes (private or public) can be random.


## Sobre a clonagem / About cloning

[PT-BR]
Clone a vontade, use a vontade, MAS NUNCA use com fins lucrativos/comerciais.


[EN-US]
Clone at will, use at will, BUT NEVER use for profit/commercial purposes.they dont show in this folder.